const getDB = require('../../db');

const { generateError } = require('../../../helpers');

const deleteVoteQuery = async (idUser, id) => {
  let connection;
  console.log(idUser, id);
  try {
    connection = await getDB();

    // Comprobamos que el usuario haya dado like al tweet.
    const [votes] = await connection.query(
      `SELECT id FROM votes WHERE idUser = ? AND idEntry = ?`,
      [idUser, id]
    );

    if (votes.length < 1) {
      throw generateError('Like no encontrado', 404);
    }

    await connection.query(
      `DELETE FROM votes WHERE idUser = ? AND idEntry = ?`,
      [idUser, id]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteVoteQuery;
