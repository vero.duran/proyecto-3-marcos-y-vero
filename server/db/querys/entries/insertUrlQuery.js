const getDB = require('../../db');

const insertUrlQuery = async (url, title, description, idUser) => {
  let connection;

  try {
    connection = await getDB();

    const [newEntry] = await connection.query(
      `
                INSERT INTO entries (url, title, description, idUser, createdAt)
                VALUES (?, ?, ?, ?, ?)
            `,
      [url, title, description, idUser, new Date()]
    );
    return newEntry.insertId;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = insertUrlQuery;
