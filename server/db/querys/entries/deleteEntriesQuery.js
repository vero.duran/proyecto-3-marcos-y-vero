const getDB = require('../../db');

const deleteEntriesQuery = async (id) => {
  let connection;

  try {
    connection = await getDB();

    await connection.query(`DELETE FROM votes WHERE idEntry = ?`, [id]);

    await connection.query(`DELETE FROM entries WHERE id = ?`, [id]);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteEntriesQuery;
