const getDB = require('../../db');

const selectAllEntriesQuery = async (idUser, keyword = '') => {
  let connection;

  try {
    connection = await getDB();

    const [entries] = await connection.query(
      ` 
        SELECT 
          E.*,
          U.name AS user,
          IFNULL(E.idUser = ?, 0) AS owner,
          COUNT(V.id) AS votes,
          BIT_OR(V.idUser = ?) AS votedByMe
        FROM entries AS E
        JOIN users U ON U.id = E.idUser
        LEFT JOIN votes V ON V.idEntry = E.id 
        WHERE E.title LIKE ? OR E.description LIKE ?
        GROUP BY E.id
        ORDER BY createdAt DESC
      `,
      [idUser, idUser, `%${keyword}%`, `%${keyword}%`]
    );

    return entries;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = selectAllEntriesQuery;
