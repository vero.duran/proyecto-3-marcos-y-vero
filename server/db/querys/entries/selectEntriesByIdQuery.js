const getDB = require('../../db');

const { generateError } = require('../../../helpers');

const selectEntriesByIdQuery = async (idUser, identries) => {
  let connection;

  try {
    connection = await getDB();

    const [entries] = await connection.query(
      `
        SELECT 
          E.*, 
          IFNULL(E.idUser = ?, 0) AS owner
        FROM entries E
        WHERE id = ? 
      `,
      [idUser, identries]
    );

    if (entries.length < 1) {
      generateError('Url no encontrada', 404);
    }

    return entries[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = selectEntriesByIdQuery;
