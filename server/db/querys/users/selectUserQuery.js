const getDB = require('../../db');
const bcrypt = require('bcrypt');

const { generateError } = require('../../../helpers');

const insertUserQuery = async (email, password, name) => {
  let connection;

  try {
    connection = await getDB();

    const [users] = await connection.query(
      `SELECT id FROM users WHERE email = ?`,
      [email]
    );

    if (users.length > 0) {
      generateError('Ya existe un usuario con ese email', 409);
    }

    const hashPass = await bcrypt.hash(password, 10);

    await connection.query(
      `INSERT INTO users (name, email, password, createdAt) VALUES (?, ?, ?, ?)`,
      [name, email, hashPass, new Date()]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = insertUserQuery;
