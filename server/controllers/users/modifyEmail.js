'use strict';

const getDB = require('../../db/db');

const modifyEmail = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { email } = req.body;

    // Comprobar que no exista otro usuario con el nuevo email
    const [existingEmail] = await connection.query(
      `
        SELECT id
        FROM users
        WHERE email=?
      `,
      [email]
    );

    if (existingEmail.length > 0) {
      const error = new Error(
        'Ya existe un usuario con el email proporcionado en la base de datos'
      );
      error.statusCode = 409;
      throw error;
    }

    // Actualizar los datos finales

    await connection.query(
      `
        UPDATE users
        SET email=?
        WHERE id=?
      `,
      [email, req.userInfo.id]
    );

    res.send({
      status: 'ok',
      message: 'Email actualizado',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};
module.exports = modifyEmail;
