const selectUserByIdQuery = require('../../db/querys/users/selectUserByIdQuery');

const getOwnUser = async (req, res, next) => {
  try {
    const user = await selectUserByIdQuery(req.userInfo.id);

    res.send({
      status: 'ok',
      data: {
        user,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = getOwnUser;
