const newUser = require('./newUser');
const loginUser = require('./loginUser');
const getOwnUser = require('./getOwnUser');
const modifyPassword = require('./modifyPassword');
const modifyEmail = require('./modifyEmail');
const modifyName = require('./modifyName');

module.exports = {
  newUser,
  loginUser,
  getOwnUser,
  modifyPassword,
  modifyEmail,
  modifyName,
};
