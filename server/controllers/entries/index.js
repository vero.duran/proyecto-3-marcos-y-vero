const newEntry = require('./newEntry');
const listEntry = require('./listEntry');
const deleteEntry = require('./deleteEntry');
const votes = require('./votes');
const deleteVote = require('./deleteVote');

module.exports = { newEntry, listEntry, deleteEntry, votes, deleteVote };
