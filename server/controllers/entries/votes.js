'use strict';

const getDB = require('../../db/db');
const { generateError } = require('../../helpers');

const vote = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { id } = req.params;
    const userId = req.userInfo.id;

    // comprobar si estoy votando mi entry
    const [entries] = await connection.query(
      `
        SELECT idUser
        FROM entries
        WHERE id=?
      `,
      [id]
    );

    if (entries[0].idUser === userId) {
      generateError('No puedes votar tu propia entrada', 403);
    }

    // compruebo que el usuario no votara anteriormente la entrada
    const [existingVote] = await connection.query(
      `
        SELECT id
        FROM votes
        WHERE idUser = ? AND idEntry = ?
      `,
      [userId, id]
    );

    if (existingVote.length > 0) {
      generateError('Ya votaste la entrada', 403);
    }

    // añado voto en la tabla
    await connection.query(
      `
      INSERT INTO votes (idUser, idEntry, createdAt)
      VALUES (?, ?, ?)
    `,
      [userId, id, new Date()]
    );

    res.status(200).send({
      status: 'ok',
      message: 'Entrada votada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = vote;
