const selectEntriesByIdQuery = require('../../db/querys/entries/selectEntriesByIdQuery');
const deleteEntriesQuery = require('../../db/querys/entries/deleteEntriesQuery');

const deleteEntry = async (req, res, next) => {
  try {
    const { id } = req.params;

    // Obtenemos la info del tweet.
    await selectEntriesByIdQuery(req.userInfo?.id, id);

    // Borramos el tweet.
    await deleteEntriesQuery(id);

    res.send({
      status: 'ok',
      message: 'Url eliminada',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = deleteEntry;
