const deleteVoteQuery = require('../../db/querys/entries/deleteVoteQuery');

const deleteVote = async (req, res, next) => {
  try {
    const { id } = req.params;

    await deleteVoteQuery(req.userInfo?.id, id);

    res.send({
      status: 'ok',
      message: 'Like eliminado',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = deleteVote;
