const jwt = require('jsonwebtoken');

const { generateError } = require('../helpers');

const generateToken = async (req, res, next) => {
  try {
    const { authorization } = req.headers;

    if (!authorization) {
      generateError('Falta la cabecera de autenticación', 400);
    }

    let tokenInfo;

    try {
      tokenInfo = jwt.verify(authorization, process.env.SECRET);
    } catch {
      generateError('Token incorrecto', 401);
    }

    // guardamos la información del token.
    req.userInfo = tokenInfo;

    next();
  } catch (err) {
    next(err);
  }
};

module.exports = generateToken;
