const generateError = (msg, code) => {
  const err = new Error(msg);
  err.statusCode = code;
  throw err;
};

module.exports = { generateError };
