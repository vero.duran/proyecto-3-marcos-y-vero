import './App.css';

import Header from './components/Header/Header';
import InfoUser from './components/InfoUser/InfoUser';
import Register from './components/Register/Register';
import Login from './components/Login/Login';
import Footer from './components/Footer/Footer';
import EntrieCreate from './components/EntrieCreate/EntrieCreate';
import { Routes, Route } from 'react-router-dom';
import ListEntrie from './components/ListEntrie/ListEntrie';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<ListEntrie />} />
        <Route path="/infoUser" element={<InfoUser />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/message" element={<EntrieCreate />} />
        <Route path="*" element={<ListEntrie />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
