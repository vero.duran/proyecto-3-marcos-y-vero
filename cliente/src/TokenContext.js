import { createContext, useState, useContext, useEffect } from 'react';

const TokenContext = createContext(null);

export const TokenProvider = ({ children }) => {
  const [token, setToken] = useState(localStorage.getItem('token'));
  const [user, setUser] = useState(null);

  useEffect(() => {
    const userData = async () => {
      try {
        const res = await fetch('http://localhost:4000/users', {
          headers: {
            Authorization: token,
          },
        });
        const body = await res.json();

        if (body.status === 'error') {
          alert(body.message);
          setToken(null);
        } else {
          setUser(body.data.user);
        }
      } catch (err) {
        console.error(err);
      }
    };

    if (token) userData();
  }, [token]);

  const setTokenInLocalStorage = (newToken) => {
    if (!newToken) {
      localStorage.removeItem('token');
      setUser(null);
    } else {
      localStorage.setItem('token', newToken);
    }
    setToken(newToken);
  };

  return (
    <TokenContext.Provider
      value={{ token, setToken: setTokenInLocalStorage, user, setUser }}
    >
      {children}
    </TokenContext.Provider>
  );
};

export const useToken = () => {
  return useContext(TokenContext);
};
