import './EntrieSearch.css';
import { useState } from 'react';
import { useToken } from '../../TokenContext';

const EntrieSearch = ({ setEntries }) => {
  const [word, setWord] = useState('');
  const [loading, setLoading] = useState(false);
  const { token } = useToken();

  const handleSubmit = async (e) => {
    e.preventDefault();

    setLoading(true);

    try {
      const res = await fetch(`http://localhost:4000/entries?keyword=${word}`, {
        headers: {
          Authorization: token,
        },
      });
      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        setEntries(body.data.entries);
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
      setWord('');
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        value={word}
        onChange={(e) => setWord(e.target.value)}
        type="text"
      />
      <button disabled={loading}>👀 Buscar 🔍</button>
    </form>
  );
};

export default EntrieSearch;
