import { NavLink } from 'react-router-dom';
import { useToken } from '../../TokenContext';

import './Header.css';

const Header = () => {
  const { token, setToken, user } = useToken();

  return (
    <header className="cabeceraNav">
      <h1 class="tooltip">
        <span class="tooltiptext">Volver al inicio</span>
        <NavLink to="/"> Busca tu URL </NavLink>
      </h1>
      <nav>
        <div className="nombreUsuario">
          {token && user && <p>@{user.name}</p>}
        </div>
        {!token && (
          <div className="button">
            <NavLink to="/login">🔒Login</NavLink>
          </div>
        )}
        {!token && (
          <div className="button">
            <NavLink to="/register">📝Registro</NavLink>
          </div>
        )}
        {token && (
          <div className="button2">
            <NavLink to="/infoUser">Modificar Usuario</NavLink>
          </div>
        )}
        {token && (
          <div className="button2">
            <NavLink to="/message">Mensaje</NavLink>
          </div>
        )}
        {token && (
          <div className="button2" onClick={() => setToken(null)}>
            <p>Cerrar Sesión</p>
          </div>
        )}
      </nav>
    </header>
  );
};

export default Header;
