import { useEffect, useState } from 'react';
import { useToken } from '../../TokenContext';
import Entrie from '../Entrie/Entrie';
import EntrieSearch from '../EntrieSearch/EntrieSearch';

import './ListEntrie.css';

const ListEntrie = ({ search }) => {
  const { token } = useToken();
  const [entries, setEntries] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch('http://localhost:4000/entries', {
          headers: {
            Authorization: token,
          },
        });
        const body = await res.json();

        if (body.status === 'error') {
          alert(body.message);
        } else {
          setEntries(body.data.entries);
        }
      } catch (err) {
        console.error(err);
      }
    };
    if (token) {
      fetchData();
    }
  }, [token]);

  return (
    <main>
      {token && (
        <>
          <EntrieSearch setEntries={setEntries} />
          <ul className="entrie-list">
            {entries &&
              entries.map((entrie) => {
                return (
                  <Entrie
                    entrie={entrie}
                    entries={entries}
                    setEntries={setEntries}
                    key={entrie.id}
                  />
                );
              })}
          </ul>
        </>
      )}
      {!token && (
        <h1>
          𝔸𝕙𝕝𝕒 𝕎 𝕊𝕒𝕙𝕝𝕒 𝕄𝕀ℝË 𝕊𝔼 𝕍𝕁𝔼ℕ 𝕊𝔼𝕃𝔸𝕄𝔸𝕋 𝔻𝔸𝕋𝔸ℕ𝔾 𝔹𝔼ℕ𝕍𝔼ℕ𝕌𝕋𝕆 𝕍Ä𝕃𝕂𝕆𝕄𝕄𝔼ℕ
          𝕎𝕚𝕝𝕝𝕜𝕠𝕞𝕞𝕖𝕟 𝔹𝕀𝔼ℕ𝕍𝔼ℕ𝕀𝔻𝕆 𝕎𝔼𝕃ℂ𝕆𝕄𝔼 𝔹𝕒𝕣𝕦𝕔𝕙 ℍ𝕒𝕓𝕒 𝔸𝕝𝕠𝕙𝕒 𝔹𝕖𝕞-𝕧𝕚𝕟𝕕𝕠 𝕍𝔼𝕃𝕂𝕆𝕄𝕀ℕ
          𝔹𝔼ℕ𝕍𝕀𝔻𝕆 𝕍𝕖𝕝𝕜𝕠𝕞𝕞𝕖𝕟 𝕋𝕖𝕣𝕖 𝕋𝕦𝕝𝕖𝕞𝕒𝕤𝕥
        </h1>
      )}
      <h2>𝕃𝕆𝔾𝕌𝔼𝔸𝕋𝔼 ℙ𝔸ℝ𝔸 𝕍𝔼ℝ 𝔼𝕃 ℂ𝕆ℕ𝕋𝔼ℕ𝕀𝔻𝕆</h2>
    </main>
  );
};

export default ListEntrie;
