import { useState } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import { useToken } from '../../TokenContext';
import './EntrieCreate.css';

const EntrieCreate = () => {
  const { token } = useToken();
  const navigate = useNavigate();

  const [title, setTitle] = useState('');
  const [url, setUrl] = useState('');
  const [description, setDescription] = useState('');
  const [loading, setLoading] = useState(false);

  if (!token) return <Navigate to="/" />;

  const handleSubmit = async (e) => {
    e.preventDefault();

    setLoading(true);

    try {
      const res = await fetch('http://localhost:4000/entries', {
        method: 'post',
        headers: {
          Authorization: token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ title, url, description }),
      });
      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        navigate('/');
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };

  return (
    <main className="entrie-create">
      <form onSubmit={handleSubmit}>
        <input
          value={title}
          type="text"
          placeholder="Introduce el título de la URL"
          onChange={(e) => setTitle(e.target.value)}
          required
        />
        <input
          value={url}
          onChange={(e) => setUrl(e.target.value)}
          type="url"
          id="url"
          placeholder="https://example.com"
          pattern="https://.*"
          required
        />
        <textarea
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          placeholder="Introduce la descripción"
          required
        />
        <button disabled={loading}>Enviar</button>
      </form>
    </main>
  );
};

export default EntrieCreate;
