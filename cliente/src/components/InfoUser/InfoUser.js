import { useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';
import { useToken } from '../../TokenContext';

import './InfoUser.css';

const InfoUser = () => {
  const { token, user } = useToken();

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [oldPwd, setOldPwd] = useState('');
  const [newPwd, setNewPwd] = useState('');

  const [loading] = useState(false);

  useEffect(() => {
    if (user) {
      setName(user.name);
      setEmail(user.email);
    }
  }, [user]);

  if (!token) return <Navigate to="/" />;

  const changeName = async () => {
    try {
      const res = await fetch('http://localhost:4000/users/name', {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          name,
        }),
      });

      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message); //gestionar los erroressss!!!!
      } else {
        alert('Nombre actualizado');
      }
    } catch (err) {
      console.error(err);
    }
  };

  const changeEmail = async () => {
    try {
      const res = await fetch('http://localhost:4000/users/email', {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          email,
        }),
      });
      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message); //gestionar los erroressss!!!!
      } else {
        alert('Email actualizado');
      }
    } catch (err) {
      console.error(err);
    }
  };

  const changePass = async () => {
    try {
      const res = await fetch('http://localhost:4000/users/password', {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          newPwd,
          oldPwd,
        }),
      });

      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message); //gestionar los erroressss!!!!
      } else {
        alert(body.message);
      }
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <main>
      <div>
        <input
          type="text"
          id="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <button onClick={() => changeName(name)} disabled={loading}>
          <p>✍🏼</p>
        </button>
      </div>

      <div>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <button onClick={() => changeEmail(user.email)} disabled={loading}>
          <p>✍🏼</p>
        </button>
      </div>

      <div>
        <input
          placeholder="Introduce la contraseña antigua"
          type="password"
          id="old-pwd"
          value={oldPwd}
          onChange={(e) => setOldPwd(e.target.value)}
        />
        <button> 🗝️</button>
      </div>
      <div>
        <input
          placeholder="Introduce la contraseña nueva"
          type="password"
          id="new-pwd"
          value={newPwd}
          onChange={(e) => setNewPwd(e.target.value)}
        />
        <button onClick={() => changePass()} disabled={loading}>
          <p>🔑</p>
        </button>
      </div>
    </main>
  );
};

export default InfoUser;
