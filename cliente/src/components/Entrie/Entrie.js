import { useState } from 'react';
import { useToken } from '../../TokenContext';
import './Entrie.css';

const Entrie = ({ entrie, entries, setEntries }) => {
  const { token } = useToken();

  const [loading] = useState();

  const voteEntrie = async (e, idEntry, votedByMe) => {
    e.target.classList.toggle('vote');

    try {
      let method = votedByMe ? 'delete' : 'post';

      const res = await fetch(
        `http://localhost:4000/entries/${idEntry}/votes`,
        {
          method,
          headers: {
            Authorization: token,
          },
        }
      );

      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        setEntries(
          entries.map((entrie) => {
            if (entrie.id === idEntry) {
              // Comprobamos si la entrie tiene la clase "vote".
              const voteClass = e.target.classList.contains('vote');

              if (voteClass) {
                entrie.votes++;
              } else {
                entrie.votes--;
              }

              // Invertimos el valor de votedByMe.
              entrie.votedByMe = !entrie.votedByMe;
            }

            return entrie;
          })
        );
      }
    } catch (err) {
      console.error(err);
    }
  };

  const DeleteEntrie = async (idEntry) => {
    //setLoading(true);

    // eslint-disable-next-line no-restricted-globals
    if (confirm('¿Deseas eliminar la entrada?')) {
      try {
        const res = await fetch(`http://localhost:4000/entries/${idEntry}`, {
          method: 'delete',
          headers: {
            Authorization: token,
          },
        });

        const body = await res.json();

        if (body.status === 'error') {
          alert(body.message);
        } else {
          setEntries(entries.filter((entrie) => entrie.id !== idEntry));
        }
      } catch (err) {
        console.error(err);
      }
    }
  };

  //setLoading(false);

  return (
    <li className="entrie">
      <header>
        <p className="nombreUsuario">@{entrie.user}</p>
        <time dateTime={entrie.createdAt}>
          {new Date(entrie.createdAt).toLocaleDateString('es-ES', {
            hour: '2-digit',
            minute: '2-digit',
            day: '2-digit',
            month: '2-digit',
            year: '2-digit',
          })}
        </time>
      </header>
      <div className="contenidoEntrada">
        <div>
          <p>{entrie.title}</p>
        </div>
        <div>
          <p>{entrie.Url}</p>
        </div>
        <div>
          <p>{entrie.description}</p>
        </div>
      </div>
      <div className="footerEntrada">
        <div
          className={`heart ${token && entrie.votedByMe && 'vote'}`}
          onClick={(e) => voteEntrie(e, entrie.id, entrie.votedByMe)}
          disabled={loading}
        ></div>
        <p className="numeroVotos">{entrie.votes}</p>
        {token && entrie.owner === 1 && (
          <button onClick={() => DeleteEntrie(entrie.id)} disabled={loading}>
            Eliminar
          </button>
        )}
      </div>
    </li>
  );
};

export default Entrie;
